CREATE TABLE IF NOT EXISTS dm_terrorism(
	number_of_crime UNSIGNED_LONG ,
	sum_of_failed_op_terrorist UNSIGNED_LONG ,
	sum_of_suicide_terrorist UNSIGNED_LONG ,
	targtype1_txt VARCHAR NOT null,
	attacktype1_txt VARCHAR NOT null,
	country VARCHAR NOT null,
	year VARCHAR NOT null,
	month VARCHAR NOT null,
	day VARCHAR NOT null,
	date_insertion VARCHAR NOT null,
	CONSTRAINT rowkey PRIMARY KEY(targtype1_txt,attacktype1_txt,country,year,month,day,date_insertion)
)