
name := "dm_terrorism_project"

version := "0.1"

scalaVersion := "2.11.12"
// https://mvnrepository.com/artifact/com.hortonworks.hive/hive-warehouse-connector
//resolvers += "Hortonworks Repository" at "http://repo.hortonworks.com/content/repositories/releases/"

//libraryDependencies += "com.hortonworks.hive" %% "hive-warehouse-connector" % "1.0.0.3.1.4.0-315"
//libraryDependencies += "com.hortonworks.hive" %% "hive-warehouse-connector" % "1.0.0.3.0.0.0-1574"
// https://mvnrepository.com/artifact/org.apache.phoenix/phoenix-spark
libraryDependencies += "org.apache.spark" %% "spark-core" % "2.3.2"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.3.2"
assemblyJarName in assembly := "dm_terrorism_project.jar"
mappings in (Compile,packageSrc) := (managedSources in Compile).value map (s => (s,s.getName))
assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}