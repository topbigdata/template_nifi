import com.hortonworks.hwc.HiveWarehouseSession
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{sum, count}
object dm_terrorism {
  def main(args: Array[String]): Unit = {
    val spark= SparkSession.builder()
      .appName("dm_terrorism")
      .getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    val hive = HiveWarehouseSession.session(spark).build()

    val dl_terrorism = hive.table("datalake.dl_terrorism").filter("date_insertion = CURRENT_DATE - interval '1' day")

    val aggDM = dl_terrorism.groupBy("country_txt","targtype1_txt","attacktype1_txt","iyear","imonth","iday","date_insertion")
      .agg(sum("success"),sum("suicide"),count("eventid"))
      .withColumnRenamed("sum(success)","NUMBER_OF_CRIME")
      .withColumnRenamed("sum(suicide)","SUM_OF_FAILED_OP_TERRORIST")
      .withColumnRenamed("count(eventid)","SUM_OF_SUICIDE_TERRORIST")
      .withColumnRenamed("country_txt","COUNTRY")
      .withColumnRenamed("iyear","YEAR")
      .withColumnRenamed("imonth","MONTH")
      .withColumnRenamed("iday","DAY")

    aggDM.write.format("org.apache.phoenix.spark").mode("overwrite").option("table", "dm_terrorism")
      .option("zkUrl", "master1.localdomain,master2.localdomain,worker1.localdomain:/hbase-unsecure:2181")
      .save()

    spark.close()
    hive.close()
  }
}
